import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  title: string = "";

  addItem(newItem: any) {
    return this.title = newItem;
  }

  constructor() { }

  ngOnInit(): void {
  }

}

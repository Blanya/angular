import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss']
})
export class StepComponent implements OnInit {

  val: string = "";

  @Output()

  giveTitle: EventEmitter<string> = new EventEmitter;

  give(value: any)
  {
    this.val = value.selectedStep.label;
    this.giveTitle.emit(this.val);
  }
  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  id: number = 0;
  title: string = "Détails";
  products: any[] = [];

  constructor(private headerService: HeaderService, private dataService: DataService, public aRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.title);
    this.products = this.dataService.getData();
    this.id = this.aRoute.snapshot.params['id'];
    
  }

}

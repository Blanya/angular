import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title: string = "Accueil"; 
  products: any[] = [];

  constructor(private headerService: HeaderService, private dataService: DataService, private route: Router) {
    this.products = dataService.getData();
   }

  ngOnInit(): void {
    this.headerService.changeTitre(this.title);
  }

  navigate(i: number)
  {
    this.route.navigate(['/products', i]);
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TrimPipe } from './tools/trim.pipe';
import { CamelPipe } from './tools/camel.pipe';
import { PageComponent } from './component/page/page.component';

@NgModule({
  declarations: [
    AppComponent,
    TrimPipe,
    CamelPipe,
    PageComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camel'
})
export class CamelPipe implements PipeTransform {

  val : string = "";
  splitTable : string[] = [];

  transform(value: string): string {

    this.splitTable = value.split(" ");

    this.splitTable.forEach(x => {
      
      if(x == this.splitTable[0])
      {
        for (let i = 0; i < x.length; i++) {
          this.val += x[i];
        }
      }
      else
      {
        this.val += x[0].toUpperCase();

        for (let i = 1; i < x.length; i++) 
        {
          this.val += x[i];
        }  
      }
    });

    return this.val;
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim'
})
export class TrimPipe implements PipeTransform {

  val: string = "";

  transform(value: string): string {

    value.length>10 ? this.val = value.substring(0, 10) + "..." : this.val = value;

    return this.val;
  }

}

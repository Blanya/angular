import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  text : string[] = ["chaine de caractère", "testons ensemble", "hola chica", "muy bien"]

  constructor() { }


  ngOnInit(): void {
  }

}

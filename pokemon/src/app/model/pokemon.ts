export interface Pokemon {
    name: string;
    id: number;
    img : string;
}

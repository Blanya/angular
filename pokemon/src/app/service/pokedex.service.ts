import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Pokemon } from '../model/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  url : string = 'https://pokeapi.co/api/v2/pokemon/';
  urlImg : string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'; 
  id: number = 0;

  constructor(private http: HttpClient) { }

  getPokemons(offset: number, limit: number)
  {

    limit>200? limit = 200 : limit= limit;

    return this.http.get<any>(`${this.url}?offset=${offset}&limit=${limit}`)
    .toPromise() 
    .then(response => response.results)
    .then(items => items.map((item: any, idx: any) => {

      const id: number = idx + offset + 1;

      return {
        name: item.name,
        img: `${this.urlImg}${id}.png`,
        id
      };
  }));}
}

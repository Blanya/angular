import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/model/pokemon';
import { PokedexService } from 'src/app/service/pokedex.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  pokemon: Pokemon[] = [];
  limit: number = 10;
  offset: number = 0;
  fstPokemons : boolean = true;

  constructor(private pokeServ: PokedexService) { }

  ngOnInit(): void {

    this.pokeServ.getPokemons(this.offset, this.limit).then((poke)=>
    {
      this.pokemon = poke;
    })
  }

  next()
  {
    this.offset += 10;
    this.pokeServ.getPokemons(this.offset, this.limit).then((pokemons) =>
    this.pokemon = pokemons),
    this.fstPokemons = false
  }

  prev()
  {
    this.offset -= 10; 
    this.offset == 0 ? this.fstPokemons = true : this.fstPokemons = false;
    this.pokeServ.getPokemons(this.offset, this.limit).then((pokemons) =>
    this.pokemon = pokemons)
  }
}

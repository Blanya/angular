import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { HeaderComponent } from './shared/header/header.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthentificationComponent } from './component/authentification/authentification.component';
import { LoginFormComponent } from './shared/login-form/login-form.component';
import { DefaultLayoutComponent } from './component/default-layout/default-layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterFormComponent } from './shared/register-form/register-form.component';
import { HttpClientModule } from '@angular/common/http';
import { ScannerComponent } from './component/scanner/scanner.component';
import { ResultatComponent } from './component/resultat/resultat.component';
import { NutriscorePipe } from './tools/nutriscore.pipe';
import { SpecificitePipe } from './tools/specificite.pipe';
import { ModalComponent } from './modal/modal/modal.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { ScanEanComponent } from './shared/scan-ean/scan-ean.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    AuthentificationComponent,
    LoginFormComponent,
    DefaultLayoutComponent,
    RegisterFormComponent,
    ScannerComponent,
    ResultatComponent,
    NutriscorePipe,
    SpecificitePipe,
    ModalComponent,
    ScanEanComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule, 
    HttpClientModule,
    ZXingScannerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

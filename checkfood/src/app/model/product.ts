export interface Product {
    name: string;
    ean: string;
    brand : string;
    cal: string; 
    allergens: [];
    carbon: number;
    img: string;
    ingredients : string;
    nutriscore: string;
    specificites: [];
}

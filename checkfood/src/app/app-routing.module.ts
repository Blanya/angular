import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { HomeComponent } from './component/home/home.component';
import { AuthentificationComponent } from './component/authentification/authentification.component';
import { DefaultLayoutComponent } from './component/default-layout/default-layout.component';
import { LoginFormComponent } from './shared/login-form/login-form.component';
import { RegisterFormComponent } from './shared/register-form/register-form.component';
import { ScannerComponent } from './component/scanner/scanner.component';
import { AuthGuardService } from './service/auth-guard.service';
import { ResultatComponent } from './component/resultat/resultat.component';

const routes: Routes = [
  {path: '', component: DefaultLayoutComponent, children: [
    {path: '', component: HomeComponent, pathMatch: 'full'},
    {path: 'authentification', component: AuthentificationComponent},
    {path: 'login', component: LoginFormComponent},
    {path: 'register', component: RegisterFormComponent},
    {path: 'scanner', component: ScannerComponent, canActivate: [AuthGuardService]},
    {path: 'produits', children:[
      {path: ':id', component: ResultatComponent, }
    ], canActivate: [AuthGuardService]},
    ]},
  {path: '**', component: DefaultLayoutComponent, children: [
    {path: '', component: HomeComponent, pathMatch: 'full'}
  ]}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

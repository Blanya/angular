import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {

  saisie: string = "Scanner";

  scannerForm!: FormGroup;
  saisir: boolean = true;

  n: any;

  constructor(private fb: FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.scannerForm = this.fb.group({
      ean: this.fb.control('', [Validators.required,
      Validators.pattern("^[0-9]{13}$")])
    })
  }

  get ean()
  {
    return this.scannerForm.get('ean');
  }

  validate()
  {
    if(this.scannerForm.valid)
    {
      this.n = this.scannerForm.value;
      this.n = this.n.ean;
    }
    this.route.navigate(['/produits', this.n]);
  }

  change()
  {
    this.saisir = !this.saisir;
    this.saisir? this.saisie = "Scanner" : this.saisie = "Saisie Manuelle";
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-resultat',
  templateUrl: './resultat.component.html',
  styleUrls: ['./resultat.component.scss']
})
export class ResultatComponent implements OnInit {
  
  id: number = 0;
  produit : Product;
  status: boolean = true;

  constructor(private ar: ActivatedRoute, private product: ProductService) { }

  ngOnInit(): void {
    this.id = this.ar.snapshot.params['id'];
    this.product.getProduct(this.id).subscribe((product)=>{
      if(product.status == 1)
      {
        this.produit= {name: product.product.product_name_fr,
                      allergens: product.product.allergens_from_ingredients,
                      ean : product.product._id, 
                      brand : product.product.brands,
                      cal: product["product"]['nutriments']['energy-kcal'],
                      nutriscore : product.product.nutriscore_grade,
                      ingredients : product.product.ingredients_text_fr,
                      img: product.product.image_url,
                      carbon: product.product.carbon_footprint_percent_of_known_ingredients,
                      specificites: product.product.  ingredients_analysis_tags}
        this.status = true;
        console.log(this.produit.cal)
      }
      else
      {
        this.status = false;
      }
    })
  }

}

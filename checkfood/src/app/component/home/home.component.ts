import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //Carousel
  images: string[] = ["../../../assets/img/shelf-3840441_1920.jpg", "../../../assets/img/grocery-list-749302_1920.jpg", "../../../assets/img/watermelon-2367029_1920.jpg"];

  constructor() { }

  ngOnInit(): void {
  }

}

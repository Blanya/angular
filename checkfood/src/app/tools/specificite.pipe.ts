import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'specificite'
})
export class SpecificitePipe implements PipeTransform {

  val: string[] = [];
  palm: string = '';
  vegan : string = '';
  vege : string = '';

  transform(value: string[]): string[]{

    for (let i = 0; i < value.length; i++) {

      if(value[i].includes("palm"))
      {
        if(value[i] == "en:palm-oil-free")
        {
          this.palm = "Sans huile de palme";
        }
        else if( value[i]== "en:palm-oil")
        {
          this.palm = "Contient de l'huile de palme";
        }
        else
        {
          this.palm = "";
        }
      }
      if(value[i].includes("vegan"))
      {
        if(value[i] == "en:vegan")
        {
          this.vegan = "Vegan"
        }
        else if(value[i] == "en:non-vegan")
        {
          this.vegan = "non-vegan";
        }
        else
        {
          this.vegan = "";
        }
      }
      if(value[i].includes("vegetarian"))
      {
        if(value[i] == "en:vegetarian")
        {
          this.vege = "Vegetarien"
        }
        else if(value[i] == "en:non-vegetarian")
        {
          this.vege = "Non-vegetarian"
        }
        else
        {
          this.vege = "";
        }
      }
    }

    this.val = [this.palm, this.vegan, this.vege];
    return this.val;

  }

}

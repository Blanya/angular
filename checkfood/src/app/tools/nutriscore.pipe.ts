import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nutriscore'
})
export class NutriscorePipe implements PipeTransform {

  val: string= "";

  transform(value: string): string {
    switch (true) {
      case value == "a":
        this.val = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Nutri-score-A.svg/640px-Nutri-score-A.svg.png";
        break;
      case value == "b":
        this.val = "https://upload.wikimedia.org/wikipedia/commons/4/4e/Nutri-score-B.svg";
        break;
      case value == "c":
        this.val = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Nutri-score-C.svg/640px-Nutri-score-C.svg.png";
        break;
      case value == "d":
        this.val = "https://upload.wikimedia.org/wikipedia/commons/d/d6/Nutri-score-D.svg";
        break;
      default:
        this.val = "https://upload.wikimedia.org/wikipedia/commons/8/8a/Nutri-score-E.svg"
        break;
    }
    return this.val;
  }

}

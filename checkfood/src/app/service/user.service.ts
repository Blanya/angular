import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from "../model/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string = "http://localhost:3000/users";

  constructor(private http: HttpClient) {
   }

   getUsers(): Observable<User[]>
   {
     return this.http.get<User[]>(this.url)
   } 

   getUser(id: number): Observable<User>
   {
    return this.http.get<User>(`${this.url}/${id}`);
   }

   createUser(user: User):Observable<any>
  {
    return this.http.post(this.url, user);
  }

  deleteUser(id: number): Observable<any>
  {      
    return this.http.delete(`${this.url}/${id}`);
  }
}

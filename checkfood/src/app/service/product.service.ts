import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string = "https://world.openfoodfacts.org/api/v0/product";

  constructor(private http: HttpClient) { }

  getProduct(id: number): Observable<any>
  {
    return this.http.get<any>(`${this.url}/${id}`);
  }
}

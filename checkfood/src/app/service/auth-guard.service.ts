import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private route: Router) { }

  canActivate() {
    if (localStorage.getItem('isLogged')) {
        // logged in so return true
        return true;
    }

    // not logged in so redirect to login page
    this.route.navigate(['/login']);
    return false;
}
}

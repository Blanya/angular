import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor(private modalService: NgbModal, private route: Router) { }

  ngOnInit(): void {
  }

  exit(content: any)
  {
    localStorage.removeItem('isLogged');
    this.modalService.open(content, {backdropClass: 'dark-backdrop', centered: true});
    this.route.navigate([''])
  }

}

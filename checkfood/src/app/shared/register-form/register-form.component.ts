import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  myForm!: FormGroup;
  listUsers : User[] = [];
  newUser!: User;

  constructor(private fb: FormBuilder, private user: UserService, private route: Router) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern("^([a-z\.0-9]*)\@([a-z]{2,10})\.(fr|com)$")
      ]),
      password : this.fb.control('', [
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ]),
    })

  }

  get email()
  {
    return this.myForm.get('email');
  }

  get password()
  {
    return this.myForm.get('password');
  }  

  validate()
  {
    if(this.myForm.valid)
    {
      this.newUser = this.myForm.value;
      this.user.getUsers().subscribe((users)=>
      {
        this.listUsers = users;
        if(this.listUsers.length>0)
      {
        for (let i = 0; i < this.listUsers.length; i++) 
        {
          if(this.listUsers[i].email == this.newUser.email)
          {
            alert("Ohoh, il me semble que ton adresse mail est déjà connu de nos services, connecte toi ! :)");
            break;
          }
        }
      }
      else
          {
            this.user.createUser(this.newUser).subscribe();
            alert("Bienvenue, ton inscription a bien été prise en compte, connecte toi :)");
          }
          this.route.navigate(['/login']);
      });      
    }
  }

}

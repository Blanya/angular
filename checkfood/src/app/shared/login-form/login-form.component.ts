import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  
  myForm!: FormGroup;
  listUsers : User[] = [];
  newUser!: User;
  isLogged : boolean = false;
  message: string = "";
  existingMail: boolean = false;
  unknowUser : boolean = true;
  
  constructor(private fb: FormBuilder, private user: UserService, private route: Router) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern("^([a-z\.0-9]*)\@([a-z]{2,10})\.(fr|com)$")
      ]),
      password : this.fb.control('', [
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ]),
    });

    localStorage.getItem('isLogged') == 'true'? this.unknowUser = false : this.unknowUser = true;

  }

  get email()
  {
    return this.myForm.get('email');
  }

  get password()
  {
    return this.myForm.get('password');
  }  

  validate()
  {
    if(this.myForm.valid)
    {
      this.newUser = this.myForm.value;
      this.user.getUsers().subscribe((users)=>
      {
        this.listUsers = users;

        if(this.listUsers.length>0)
      {
        for (let i = 0; i < this.listUsers.length; i++) 
        {
          if(this.listUsers[i].email == this.newUser.email)
          {
            this.existingMail = true;

            if(this.listUsers[i].password == this.newUser.password)
            {
              this.isLogged = true;
              localStorage.setItem('isLogged', 'true');
              this.route.navigate(['/scanner']);
              break;
            }
            else
            {
              this.message = "Mot de passe incorrect, veuillez réessayer avec le bon !"
            }
          }
          
        }
      }
      if(this.existingMail == false)
  {
    this.message = "Votre adresse mail ne semble pas reconnue de notre service, inscrivez-vous !"
  }
    }); 
  }
  
}

  navigate()
  {
    this.route.navigate(['scanner']);
  }
}

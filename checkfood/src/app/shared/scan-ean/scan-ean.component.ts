import { Component, OnInit, ViewChild } from '@angular/core';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { Result, BarcodeFormat } from '@zxing/library'
import { Router } from '@angular/router';

@Component({
  selector: 'app-scan-ean',
  templateUrl: './scan-ean.component.html',
  styleUrls: ['./scan-ean.component.scss']
})
export class ScanEanComponent implements OnInit {



  scanner: ZXingScannerComponent;

  resultatString: string = "";


  allowedFormats : BarcodeFormat[] = [ BarcodeFormat.EAN_13 ]

  constructor(private route: Router) { }

  ngOnInit(): void {
    
  }

  handleCodeResult(resultString: string) 
  {
      this.resultatString = resultString;
      //this.route.navigate(['/produits', this.resultatString])
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanEanComponent } from './scan-ean.component';

describe('ScanEanComponent', () => {
  let component: ScanEanComponent;
  let fixture: ComponentFixture<ScanEanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScanEanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScanEanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

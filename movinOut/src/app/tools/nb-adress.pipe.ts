import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nbAdress'
})
export class NbAdressPipe implements PipeTransform {

  val : number;

  transform(value: number): number {
    value>0 ? this.val = 1 : this.val =0;
    return this.val;
  }

}

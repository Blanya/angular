import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'adress'
})
export class AdressPipe implements PipeTransform {
  
  val : string;

  transform(value: any): string{
    this.val = `${value.num} ${value.rue} - ${value.postal} ${value.ville}`;
    return this.val;
  }

}

export interface Adresse {
    id? : number,
    num: string,
    rue: string,
    postal: number,
    ville: string,
    habitants? : number
}

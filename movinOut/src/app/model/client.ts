import { Adresse } from "./adresse";

export interface Client {
    id? : number,
    nom: string,
    prenom: string, 
    email: string,
    tel: string,
    adressId? : number,
    date? : Date,
    homme? : boolean,
}

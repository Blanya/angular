import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adresse } from '../model/adresse';

@Injectable({
  providedIn: 'root'
})
export class AdresseService {

  url: string = "http://localhost:3000/adresses";

  constructor(private http: HttpClient) { }

  getAdresses(): Observable<Adresse[]>
   {
     return this.http.get<Adresse[]>(this.url)
   } 

   getAdresse(id: number): Observable<Adresse>
   {
    return this.http.get<Adresse>(`${this.url}/${id}`);
   }

   createAdresse(user: Adresse):Observable<any>
  {
    return this.http.post(this.url, user);
  }

  deleteAdresse(id: number): Observable<any>
  {      
    return this.http.delete(`${this.url}/${id}`);
  }
  
}

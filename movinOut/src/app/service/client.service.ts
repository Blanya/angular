import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from '../model/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  url: string = "http://localhost:3000/clients";

  constructor(private http: HttpClient) { }

  getClients(): Observable<Client[]>
   {
     return this.http.get<Client[]>(this.url)
   } 

   getClient(id: number): Observable<Client>
   {
    return this.http.get<Client>(`${this.url}/${id}`);
   }

   createClient(client: Client):Observable<any>
  {
    return this.http.post(this.url, client);
  }

  deleteClient(id: number): Observable<any>
  {      
    return this.http.delete(`${this.url}/${id}`);
  }
  updateClient(id: number, client: Client): Observable<any>
  {
    return this.http.put(`${this.url}/${id}`, client);
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { NavComponent } from './shared/nav/nav.component';
import { ClientComponent } from './component/client/client.component';
import { AdresseComponent } from './component/adresse/adresse.component';
import { HttpClientModule } from '@angular/common/http';
import { AddClientComponent } from './component/add-client/add-client.component';
import { AddAdresseComponent } from './component/add-adresse/add-adresse.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdressPipe } from './tools/adress.pipe';
import { ViewClientComponent } from './component/view-client/view-client.component';
import { EditClientComponent } from './component/edit-client/edit-client.component';
import { NbAdressPipe } from './tools/nb-adress.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    ClientComponent,
    AdresseComponent,
    AddClientComponent,
    AddAdresseComponent,
    AdressPipe,
    ViewClientComponent,
    EditClientComponent,
    NbAdressPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    ReactiveFormsModule, 
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

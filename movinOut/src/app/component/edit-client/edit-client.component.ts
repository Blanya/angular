import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Adresse } from 'src/app/model/adresse';
import { Client } from 'src/app/model/client';
import { AdresseService } from 'src/app/service/adresse.service';
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent implements OnInit {

  dataAdresses: Adresse[] = [];
  myForm!: FormGroup;
  id: number = 0;
  dataClient: Client;

  constructor(private fb: FormBuilder, private adresse: AdresseService, private aR: ActivatedRoute, private client: ClientService) { }

  ngOnInit(): void {

    this.id = this.aR.snapshot.params['id'];

    this.client.getClient(this.id).subscribe((client) =>
    this.dataClient = client);
    
    this.adresse.getAdresses().subscribe((adresses)=>
    this.dataAdresses = adresses);

    this.myForm = this.fb.group({
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern("^([a-z\.0-9]*)\@([a-z]{2,10})\.(fr|com)$")
      ]),
      nom : this.fb.control('', Validators.required),
      prenom : this.fb.control('', Validators.required),
      date: this.fb.control('', Validators.required),
      homme: this.fb.control(''),
      tel: this.fb.control('', [
        Validators.required,
        Validators.pattern("^(\\+33|0|0033)[1-9][0-9]{8}$")
      ]),
      adressId: this.fb.control('', Validators.required),
    });    
  }

  ngAfterViewInit()
  {
    this.myForm.patchValue(this.dataClient)
  }

  get email()
  {
    return this.myForm.get('email');
  }

  get nom()
  {
    return this.myForm.get('nom');
  }
  
  get prenom()
  {
    return this.myForm.get('prenom');
  }

  get date()
  {
    return this.myForm.get('date');
  }

  get homme()
  {
    return this.myForm.get('homme');
  }

  get tel()
  {
    return this.myForm.get('tel');
  }

  get adressId()
  {
    return this.myForm.get('adressId')
  }

  validate()
  {
    this.client.updateClient(this.id, this.myForm.value).subscribe();
  }

}

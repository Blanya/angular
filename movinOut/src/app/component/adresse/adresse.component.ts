import { Component, OnInit } from '@angular/core';
import { Adresse } from 'src/app/model/adresse';
import { AdresseService } from 'src/app/service/adresse.service';

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.scss']
})
export class AdresseComponent implements OnInit {

  dataAdresses : Adresse[] = [];

  constructor(private adresse: AdresseService) { }

  ngOnInit(): void {
    this.adresse.getAdresses().subscribe((adresses)=>
    this.dataAdresses = adresses);
  }

}

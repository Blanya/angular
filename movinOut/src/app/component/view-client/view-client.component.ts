import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresse } from 'src/app/model/adresse';
import { Client } from 'src/app/model/client';
import { AdresseService } from 'src/app/service/adresse.service';
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls: ['./view-client.component.scss']
})
export class ViewClientComponent implements OnInit {

  dataClient: Client;
  id: number = 0;

  adress: Adresse;

  constructor(private client: ClientService, private ar: ActivatedRoute, private adresse: AdresseService, private route: Router) {
   }

  ngOnInit(): void {

    this.id = this.ar.snapshot.params['id'];
    
    this.client.getClient(this.id).subscribe((client) =>
    {
     this.dataClient = client; 
     this.adresse.getAdresse(this.dataClient.adressId).subscribe((adresse)=>
    this.adress = adresse)
    })
       
  }
  
  navigate()
  {
    this.route.navigate(['/clients', this.id, 'edit'])
  }

}

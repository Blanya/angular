import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Adresse } from 'src/app/model/adresse';
import { AdresseService } from 'src/app/service/adresse.service';
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {

  dataAdresses: Adresse[] = [];
  myForm!: FormGroup;
  
  constructor(private fb: FormBuilder, private adresse: AdresseService, private clientServ : ClientService, private route: Router) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern("^([a-z\.0-9]*)\@([a-z]{2,10})\.(fr|com)$")
      ]),
      nom : this.fb.control('', Validators.required),
      prenom : this.fb.control('', Validators.required),
      date: this.fb.control('', Validators.required),
      homme: this.fb.control(''),
      tel: this.fb.control('', [
        Validators.required,
        Validators.pattern("^(\\+33|0|0033)[1-9][0-9]{8}$")
      ]),
      adressId : this.fb.control('', Validators.required)
    });

    this.adresse.getAdresses().subscribe((adresses)=>
    this.dataAdresses = adresses)
  }

  get email()
  {
    return this.myForm.get('email');
  }

  get nom()
  {
    return this.myForm.get('nom');
  }
  
  get prenom()
  {
    return this.myForm.get('prenom');
  }

  get date()
  {
    return this.myForm.get('date');
  }

  get homme()
  {
    return this.myForm.get('homme');
  }

  get tel()
  {
    return this.myForm.get('tel');
  }

  validate()
  {
    this.clientServ.createClient(this.myForm.value);
    //this.route.navigate(['clients']);
    console.log(this.myForm.value);
  }

}

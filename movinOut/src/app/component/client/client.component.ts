import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  dataClients : Client[] = [];
  id: number = 0;

  constructor(private client: ClientService, private route: Router) { }

  ngOnInit(): void {
    this.client.getClients().subscribe((clients)=>
    this.dataClients = clients);

    console.log(this.dataClients)
  }

  view(e: any)
  {
    this.id = e.path[2].children[0].innerHTML;
    this.route.navigate(['/clients', this.id]);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-add-adresse',
  templateUrl: './add-adresse.component.html',
  styleUrls: ['./add-adresse.component.scss']
})
export class AddAdresseComponent implements OnInit {

  dataClients: Client[] = [];
  myForm!: FormGroup;
  
  constructor(private fb: FormBuilder, private client: ClientService) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      num : this.fb.control('', Validators.required),
      rue : this.fb.control('', Validators.required),
      postal : this.fb.control('', Validators.required),
      ville: this.fb.control('', Validators.required),
    });

    this.client.getClients().subscribe((clients)=>
    this.dataClients = clients)
  }

  get num()
  {
    return this.myForm.get('num');
  }
  
  get rue()
  {
    return this.myForm.get('rue');
  }

  get postal()
  {
    return this.myForm.get('postal');
  }

  get ville()
  {
    return this.myForm.get('ville');
  }

  validate()
  {

  }

}

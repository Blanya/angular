import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAdresseComponent } from './component/add-adresse/add-adresse.component';
import { AddClientComponent } from './component/add-client/add-client.component';
import { AdresseComponent } from './component/adresse/adresse.component';
import { ClientComponent } from './component/client/client.component';
import { EditClientComponent } from './component/edit-client/edit-client.component';
import { HomeComponent } from './component/home/home.component';
import { ViewClientComponent } from './component/view-client/view-client.component';

const routes: Routes = [
  {path:"", component: HomeComponent},
  {path:"clients", children: [
    {path: '', component: ClientComponent},
    {path: 'ajout', component: AddClientComponent},
    {path: ":id", children:[
      {path: '', component: ViewClientComponent},
      {path: 'edit', component: EditClientComponent}
    ]},
  ]},
  {path:"adresses", component: AdresseComponent},
  {path:"ajout-adresse", component: AddAdresseComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

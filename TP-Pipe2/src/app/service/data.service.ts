import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  tabData : any[] = [{
    nom : "Serveur",
    taille : 1,
    status : "maintenance",
    date: new Date(2019, 0, 25)
  },
  {
    nom : "Game",
    taille : 4,
    status : "disponible",
    date: new Date(2018, 4, 25)
  },
  {
    nom : "Tecna",
    taille : 2,
    status : "plein",
    date: new Date(2012, 3 ,5)
  },
  {
    nom : "ServeurBis",
    taille : 1,
    status : "disponible",
    date: new Date(2015, 3 ,4)
  }]

  constructor() {}


  getData(): any[]
  {
    return this.tabData;
  }
}

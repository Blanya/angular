import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  
  affiche: boolean = true;
  servers : any[] = [];

  constructor(private dataService: DataService) { 
  }

  change(value: any)
  {
    this.affiche = !this.affiche;

    this.affiche == true? value.target.textContent = "en caractère ?" : value.target.textContent = "en chiffre ?"
  }

  ngOnInit(): void {
    this.servers = this.dataService.getData();
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sorting'
})
export class SortingPipe implements PipeTransform {
  
  tabSort : any[] = [];

  transform(value : any[]): any[] {

    this.tabSort = value.slice().sort((a: any, b: any) => a.date - b.date);
    
    return this.tabSort;
  }

}

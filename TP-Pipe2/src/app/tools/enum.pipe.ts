import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enum'
})
export class EnumPipe implements PipeTransform {

  val : string = "";

  transform(value: number): string {
    switch (true) {
      case value == 1:
        this.val = "petit";
        break;
      case value == 2:
        this.val = "moyen";
        break;
      case value == 3:
        this.val = "grand";
        break;
      case value == 4:
        this.val = "gigantesque";
        break;
      default:
        this.val = "inconnu";
        break;
    }

    return this.val;
  }

}

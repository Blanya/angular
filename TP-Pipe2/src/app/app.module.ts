import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PageComponent } from './component/page/page.component';
import { EnumPipe } from './tools/enum.pipe';
import { SortingPipe } from './tools/sorting.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    EnumPipe,
    SortingPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

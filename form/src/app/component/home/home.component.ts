import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/service/header.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  title: string = "Home";
  myForm: FormGroup;
  user: User;
  tableStored: any;
  users: any[]= [];
  
  
  constructor(private headerService: HeaderService, private fb: FormBuilder, private route: Router) {
    
  }
  
  ngOnInit(): void {
    this.headerService.changeTitre(this.title);
    this.myForm = this.fb.group({
      genre: this.fb.control('', Validators.required),
      firstName: this.fb.control('', Validators.required),
      lastName: this.fb.control('', Validators.required),
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern("^([a-z\.0-9]*)\@([a-z]{2,10})\.(fr|com)$")
      ]),
      tel: this.fb.control('', [
        Validators.required,
        Validators.pattern("^(\\+33|0|0033)[1-9][0-9]{8}$")
      ]),
      city: this.fb.control('', Validators.required),
      zip: this.fb.control('', [
        Validators.required,
        Validators.compose([Validators.maxLength(5), Validators.minLength(5)])
      ]),
      cgu : this.fb.control('', [
        Validators.required,
        Validators.pattern('true')
      ]),
      promo: this.fb.control(""),
      contact : this.fb.control("")
    })
  };
  
  get genre()
  {
    return this.myForm.get('genre');
  }
  
  get firstName()
  {
    return this.myForm.get('firstName');
  }
  
  get lastName()
  {
    return this.myForm.get('lastName');
  }
  
  get email()
  {
    return this.myForm.get('email');
  }
  
  get tel()
  {
    return this.myForm.get('tel');
  }
  
  get city()
  {
    return this.myForm.get('tel');
  }
  
  get zip()
  {
    return this.myForm.get('tel');
  }
  
  get cgu()
  {
    return this.myForm.get('cgu');
  }
  
  get promo()
  {
    return this.myForm.get('promo');
  }
  
  get contact()
  {
    return this.myForm.get('contact');
  }
  
  validate()
  {
    console.log(this.myForm.value);
    this.user = this.myForm.value;
    console.log(this.user);
    
    if(typeof localStorage!='undefined') 
    {
      // Récupération de la valeur dans web storage
      this.tableStored = localStorage.getItem('users');
      
      // Vérification de la présence du tableau
      if(this.tableStored!=null) 
      {
        //Récupération de l'objet
        this.tableStored = JSON.parse(this.tableStored);
      } else {
        this.tableStored = [];
      }
      // Verif + Attribution
      if(Array.isArray(this.tableStored))
      {
        this.users = this.tableStored;
      }     
    }
    
    this.users.push(this.user);
    
    
    localStorage.setItem('users', JSON.stringify(this.users));
    this.route.navigate(['result']);
  }
  
}

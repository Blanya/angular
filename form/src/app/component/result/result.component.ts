import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  title: string = "Resultat";
  data: any[] = [];

  constructor(private headerService: HeaderService, private route: Router) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.title);
    this.data = JSON.parse(localStorage.getItem('users'));
  }

  delete()
  {
    localStorage.removeItem('users');
    this.data = null;
  }
}

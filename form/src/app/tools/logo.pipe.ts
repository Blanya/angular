import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'logo'
})
export class LogoPipe implements PipeTransform {

  val: any;

  transform(value: string): string {
    this.val = value;
    this.val == true ? this.val = "✅" : this.val = "❌";
    return this.val;
  }

}

export interface User {
    firstName: string;
    lastName: string;
    email: string; 
    city: string;
    zip: string;
    tel: string;
    cgu: boolean;
    promo: boolean;
    contact: boolean;
}

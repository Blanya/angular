import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'icon'
})
export class IconPipe implements PipeTransform {

  val: any;

  transform(value: string): string {
    this.val = value;
    this.val == true ? this.val = "✅ Tache effectuée" : this.val = "❌ A effectuer";
    return this.val;
  }

}

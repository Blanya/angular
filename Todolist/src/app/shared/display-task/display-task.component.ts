import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-display-task]',
  templateUrl: './display-task.component.html',
  styleUrls: ['./display-task.component.scss']
})
export class DisplayTaskComponent implements OnInit {

  @Input()
  inputFromParent : any | undefined;

  @Output()
  giveTodo : EventEmitter<[]> = new EventEmitter ;

  display()
  {
    console.log(this.inputFromParent)
  }

  delete(i: any)
  {
    this.inputFromParent.splice(Number(i), 1); 
    this.giveTodo.emit(this.inputFromParent);
  }

  changeTask(i: any)
  {
    this.inputFromParent[i].done = !this.inputFromParent[i].done;
    this.giveTodo.emit(this.inputFromParent);
  }

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {

  data : {} = {};

  @Output()

  giveTask : EventEmitter<{}> = new EventEmitter ;

  onSubmit(f: NgForm) {
     // {object with data name}
    this.data = f.value;
    console.log(this.data)
    this.giveTask.emit(this.data);
  }

  constructor() { }

  ngOnInit(): void {
  }

}

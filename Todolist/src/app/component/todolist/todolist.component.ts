import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {

  todo: any[] = [];
  map : {} = {};
  property: {} = {date: new Date, done: false};

  //Recuperer tâche ajouter depuis composant addTask

  takeTask(newTask : any)
  {
    this.map = {...newTask, ...this.property}
    this.todo.push(this.map);
  }


  takeTodo(todo : [])
  {
    this.map = todo;
    console.log(this.map)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
